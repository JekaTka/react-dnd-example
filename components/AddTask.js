import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DayPicker, { DateUtils } from 'react-day-picker';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';


class AddTask extends Component {

    static propTypes = {
        new_task: PropTypes.object.isRequired,
        addTask: PropTypes.func.isRequired,
        cancelAddTask: PropTypes.func.isRequired,
        cancelEditTask: PropTypes.func.isRequired

    };

    state = {
        //from: null,
        //to: null,
        focus: false
    };

    handleDayClick = day => {
        const range = DateUtils.addDayToRange(day, this.props.new_task);
        this.props.datesChanged({
            from: range.from,
            to: range.to,
            start_date: (range.from) ? moment(range.from).format("DD/MM/YYYY") : null,
            end_date: (range.to) ? moment(range.to).format("DD/MM/YYYY") : null
        });
        //this.setState(range);
    };

    focusDatePicker = () => {
        this.setState({ focus: true });
    };

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside.bind(this), true);
    }

    handleClickOutside(evt) {
        const area = ReactDOM.findDOMNode(this.refs.area);

        if (area && !area.contains(evt.target)) {
            this.setState({ focus: false });
        }
    }

    render(){
        //const { from, to } = this.state;
        //console.log({from, to});
        const { from, to } = this.props.new_task;

        return(
            <div className="m-b-5">
                {
                    (this.props.new_task.open) ?
                        <div className="task-form">
                            <input type="text" className={(this.props.new_task.title_error) ? "form-control task-title parsley-error" : "form-control task-title"} value={this.props.new_task.title} onChange={(e) => this.props.titleChanged(e.target.value)} placeholder="Title" />
                            <ul className="nav nav-tabs nav-task navtab-bg">
                                <li className="active">
                                    <a href="#main-info" data-toggle="tab" aria-expanded="false">
                                        <span><i className="md md-account-box" /></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#profile1" data-toggle="tab" aria-expanded="true">
                                        <span><i className="md md-subject" /></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#messages1" data-toggle="tab" aria-expanded="false">
                                        <span><i className="md md-error" /></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#settings1" data-toggle="tab" aria-expanded="false">
                                        <span><i className="md md-av-timer" /></span>
                                    </a>
                                </li>
                            </ul>
                            <div className="tab-content">
                                <div className="tab-pane active" id="main-info">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            <label>Who should do this?</label>
                                            <select className="form-control" value={this.props.new_task.owner_id} onChange={(e) => this.props.ownerChanged(e.target.value)}>
                                                {this.props.users.map((user, i) => (
                                                    <option key={"user-"+user.id} value={user.id}>{user.first_name} {user.last_name}</option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-xs-6" ref="area">
                                            <div className="task-daterange">
                                                <div className="task-daterange-input" onClick={() => this.focusDatePicker()}>
                                                    <div className="date-input">
                                                        <div className={(this.state.focus) ? "date-text date-text--focused" : "date-text"}>{(this.props.new_task.start_date) ? moment(this.props.new_task.start_date, "DD/MM/YYYY").format("MM/DD/YYYY") : 'Start date'}</div>
                                                    </div>
                                                    <div className="date-input-range">
                                                        ->
                                                    </div>
                                                    <div className="date-input">
                                                        <div className={(this.state.focus) ? "date-text date-text--focused" : "date-text"}>{(this.props.new_task.end_date) ? moment(this.props.new_task.end_date, "DD/MM/YYYY").format("MM/DD/YYYY") : 'End date'}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            {
                                                (this.state.focus) ?
                                                    <DayPicker numberOfMonths={1}
                                                               selectedDays={[from, { from, to }]}
                                                               onDayClick={(day) => this.handleDayClick(day)}
                                                               fixedWeeks />
                                                    :
                                                    ""
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane" id="profile1">
                                    <label>Description</label>
                                    <textarea className="form-control" value={this.props.new_task.description} onChange={(e) => this.props.descriptionChanged(e.target.value)} />
                                </div>
                                <div className="tab-pane" id="messages1">
                                    <label>Choose the priority of this task</label>
                                    <div>
                                        <div className={(this.props.new_task.priority == 0) ? "task-priority m-r-5 selected" : "task-priority m-r-5"} onClick={() => this.props.priorityChanged(0)}>
                                            <i className="md md-error" /> None
                                        </div>
                                        <div className={(this.props.new_task.priority == 1) ? "task-priority text-success m-r-5 selected" : "task-priority text-success m-r-5"} onClick={() => this.props.priorityChanged(1)}>
                                            <i className="md md-error" /> Low
                                        </div>
                                        <div className={(this.props.new_task.priority == 2) ? "task-priority text-warning m-r-5 selected" : "task-priority text-warning m-r-5"} onClick={() => this.props.priorityChanged(2)}>
                                            <i className="md md-error" /> Medium
                                        </div>
                                        <div className={(this.props.new_task.priority == 3) ? "task-priority text-danger m-r-5 selected" : "task-priority text-danger m-r-5"} onClick={() => this.props.priorityChanged(3)}>
                                            <i className="md md-error" /> High
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane" id="settings1">
                                    <label>Progress so far ({this.props.new_task.percentage} %)</label>
                                    <div className="percentage">
                                        <InputRange
                                            maxValue={100}
                                            minValue={0}
                                            value={this.props.new_task.percentage}
                                            onChange={percentage => this.props.percentageChanged(percentage)} />
                                    </div>
                                    <p className="text-mutted">Complete this task to mark it as 100% complete</p>
                                </div>
                            </div>
                            <div>
                                <button className="btn btn-primary waves-effect waves-light btn-sm m-r-5 m-l-10" onClick={() => this.props.saveTask(this.props.new_task)}>Save</button>
                                <button className="btn btn-inverse waves-effect waves-light btn-sm" onClick={() => this.props.cancelAddTask()}>Cancel</button>
                            </div>
                        </div>
                        :
                        <div className="text-center">
                            <button className="btn btn-success btn-sm waves-effect waves-light" onClick={() => {

                                this.props.cancelEditTask();
                                this.props.addTask();
                            }}>Add</button>
                        </div>
                }
            </div>
        );
    }
}

export default AddTask;