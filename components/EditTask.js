import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DayPicker, { DateUtils } from 'react-day-picker';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';

export default class EditTask extends Component {

    static propTypes = {
        edit_task: PropTypes.object.isRequired,
        users: PropTypes.array.isRequired,
        editTitleChanged: PropTypes.func.isRequired,
        cancelEditTask: PropTypes.func.isRequired,
        deleteTask: PropTypes.func.isRequired
    };

    state = {
        focus: false
    };

    focusDatePicker = () => {
        this.setState({ focus: true });
    };

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside.bind(this), true);
    }

    handleClickOutside(evt) {
        const area = ReactDOM.findDOMNode(this.refs.area);

        if (area && !area.contains(evt.target)) {
            this.setState({ focus: false });
        }
    }

    handleDayClick = day => {
        const range = DateUtils.addDayToRange(day, this.props.edit_task);
        this.props.editDatesChanged({
            from: range.from,
            to: range.to,
            start_date: (range.from) ? moment(range.from).format("DD/MM/YYYY") : null,
            end_date: (range.to) ? moment(range.to).format("DD/MM/YYYY") : null
        });
    };

    render(){
        const { from, to } = this.props.edit_task;

        return(
            <div>
                <div className="task-form">
                    <input type="text" className={(this.props.edit_task.title_error) ? "form-control task-title parsley-error" : "form-control task-title"} value={this.props.edit_task.title} onChange={(e) => this.props.editTitleChanged(e.target.value)} placeholder="Title" />
                    <ul className="nav nav-tabs nav-task navtab-bg">
                        <li className="active">
                            <a href="#main-info" data-toggle="tab" aria-expanded="false">
                                <span><i className="md md-account-box" /></span>
                            </a>
                        </li>
                        <li>
                            <a href="#profile1" data-toggle="tab" aria-expanded="true">
                                <span><i className="md md-subject" /></span>
                            </a>
                        </li>
                        <li>
                            <a href="#messages1" data-toggle="tab" aria-expanded="false">
                                <span><i className="md md-error" /></span>
                            </a>
                        </li>
                        <li>
                            <a href="#settings1" data-toggle="tab" aria-expanded="false">
                                <span><i className="md md-av-timer" /></span>
                            </a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane active" id="main-info">
                            <div className="row">
                                <div className="col-xs-6">
                                    <label>Who should do this?</label>
                                    <select className="form-control" value={this.props.edit_task.owner_id} onChange={(e) => this.props.editOwnerChanged(e.target.value)}>
                                        {this.props.users.map((user, i) => (
                                            <option key={"user-"+user.id} value={user.id}>{user.first_name} {user.last_name}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col-xs-6" ref="area">
                                    <div className="task-daterange">
                                        <div className="task-daterange-input" onClick={() => this.focusDatePicker()}>
                                            <div className="date-input">
                                                <div className={(this.state.focus) ? "date-text date-text--focused" : "date-text"}>{(this.props.edit_task.start_date) ? moment(this.props.edit_task.start_date, "DD/MM/YYYY").format("MM/DD/YYYY") : 'Start date'}</div>
                                            </div>
                                            <div className="date-input-range">
                                                ->
                                            </div>
                                            <div className="date-input">
                                                <div className={(this.state.focus) ? "date-text date-text--focused" : "date-text"}>{(this.props.edit_task.end_date) ? moment(this.props.edit_task.end_date, "DD/MM/YYYY").format("MM/DD/YYYY") : 'End date'}</div>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        (this.state.focus) ?
                                            <DayPicker numberOfMonths={1}
                                                       month={(this.props.edit_task.start_date) ? new Date(moment(this.props.edit_task.start_date, 'DD/MM/YYYY').format('YYYY'), (moment(this.props.edit_task.start_date, 'DD/MM/YYYY').format('M') - 1)) : null}
                                                       selectedDays={[from, { from, to }]}
                                                       onDayClick={(day) => this.handleDayClick(day)}
                                                       fixedWeeks />
                                            :
                                            ""
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane" id="profile1">
                            <label>Description</label>
                            <textarea className="form-control" value={this.props.edit_task.description} onChange={(e) => this.props.editDescriptionChanged(e.target.value)} />
                        </div>
                        <div className="tab-pane" id="messages1">
                            <label>Choose the priority of this task</label>
                            <div>
                                <div className={(this.props.edit_task.priority == 0) ? "task-priority m-r-5 selected" : "task-priority m-r-5"} onClick={() => this.props.editPriorityChanged(0)}>
                                    <i className="md md-error" /> None
                                </div>
                                <div className={(this.props.edit_task.priority == 1) ? "task-priority text-success m-r-5 selected" : "task-priority text-success m-r-5"} onClick={() => this.props.editPriorityChanged(1)}>
                                    <i className="md md-error" /> Low
                                </div>
                                <div className={(this.props.edit_task.priority == 2) ? "task-priority text-warning m-r-5 selected" : "task-priority text-warning m-r-5"} onClick={() => this.props.editPriorityChanged(2)}>
                                    <i className="md md-error" /> Medium
                                </div>
                                <div className={(this.props.edit_task.priority == 3) ? "task-priority text-danger m-r-5 selected" : "task-priority text-danger m-r-5"} onClick={() => this.props.editPriorityChanged(3)}>
                                    <i className="md md-error" /> High
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane" id="settings1">
                            <label>Progress so far ({this.props.edit_task.percentage} %)</label>
                            <div className="percentage">
                                <InputRange
                                    maxValue={100}
                                    minValue={0}
                                    value={this.props.edit_task.percentage}
                                    onChange={percentage => this.props.editPercentageChanged(percentage)} />
                            </div>
                            <p className="text-mutted">Complete this task to mark it as 100% complete</p>
                        </div>
                    </div>
                    <div>
                        <button className="btn btn-primary waves-effect waves-light btn-sm m-r-5 m-l-10" onClick={() => this.props.updateTask(this.props.edit_task)}>Save</button>
                        <button className="btn btn-danger waves-effect waves-light btn-sm m-r-5" onClick={() => this.props.deleteTask(this.props.edit_task)}>Delete</button>
                        <button className="btn btn-inverse waves-effect waves-light btn-sm" onClick={() => this.props.cancelEditTask()}>Cancel</button>
                    </div>
                </div>
            </div>
        );
    }
}