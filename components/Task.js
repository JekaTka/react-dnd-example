import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {findDOMNode} from 'react-dom';
import {DragSource, DropTarget} from 'react-dnd';
import {Popover, OverlayTrigger, Tooltip} from 'react-bootstrap';
import DayPicker, {DateUtils} from 'react-day-picker';
import InputRange from 'react-input-range';
import moment from "moment";

const cardSource = {
    beginDrag(props) {
        return {
            id: props.id,
            index: props.index
        };
    }
};

const cardTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return;
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        // Determine mouse position
        const clientOffset = monitor.getClientOffset();

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top;

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return;
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return;
        }

        // Time to actually perform the action
        props.moveCard(dragIndex, hoverIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex;
    }
};

@DropTarget('task', cardTarget, connect => ({
    connectDropTarget: connect.dropTarget()
}))
@DragSource('task', cardSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))

class Task extends Component {

    static propTypes = {
        connectDragSource: PropTypes.func.isRequired,
        connectDropTarget: PropTypes.func.isRequired,
        index: PropTypes.number.isRequired,
        isDragging: PropTypes.bool.isRequired,
        id: PropTypes.any.isRequired,
        task: PropTypes.object.isRequired,
        moveCard: PropTypes.func.isRequired,
        editTask: PropTypes.func.isRequired,
        cancelAddTask: PropTypes.func.isRequired
    };

    handleDayClick = (day, from, to, task_id) => {
        const range = DateUtils.addDayToRange(day, {from, to});

        this.props.inlineSetDates({
            id: task_id,
            start_date: (range.from) ? moment(range.from).format("DD/MM/YYYY") : null,
            end_date: (range.to) ? moment(range.to).format("DD/MM/YYYY") : null
        });
    };

    render() {
        const {task, isDragging, connectDragSource, connectDropTarget} = this.props;
        const opacity = isDragging ? 0 : 1;

        const from = (task.start_date) ? moment(task.start_date)._d : null;
        const to = (task.end_date) ? moment(task.end_date)._d : null;

        const popoverCalendar = (
            <Popover id="popover-trigger-click-root-close">
                <DayPicker numberOfMonths={1}
                           month={(task.start_date) ? new Date(moment(task.start_date).format('YYYY'), (moment(task.start_date).format('M') - 1)) : null}
                           selectedDays={[from, {from, to}]}
                           onDayClick={(day) => this.handleDayClick(day, from, to, task.id)}
                           fixedWeeks/>
            </Popover>
        );

        const popoverPriority = (
            <Popover id="popover-priority-trigger-click-root-close">
                <div>
                    <div className={(task.priority == 0) ? "task-priority m-b-5 selected" : "task-priority m-b-5"}
                         onClick={() => this.props.inlineSetPriority(task, 0)}>
                        <i className="md md-error"/> None
                    </div>
                    <div
                        className={(task.priority == 1) ? "task-priority text-success m-b-5 selected" : "task-priority text-success m-b-5"}
                        onClick={() => this.props.inlineSetPriority(task, 1)}>
                        <i className="md md-error"/> Low
                    </div>
                    <div
                        className={(task.priority == 2) ? "task-priority text-warning m-b-5 selected" : "task-priority text-warning m-b-5"}
                        onClick={() => this.props.inlineSetPriority(task, 2)}>
                        <i className="md md-error"/> Medium
                    </div>
                    <div
                        className={(task.priority == 3) ? "task-priority text-danger m-b-5 selected" : "task-priority text-danger m-b-5"}
                        onClick={() => this.props.inlineSetPriority(task, 3)}>
                        <i className="md md-error"/> High
                    </div>
                </div>
            </Popover>
        );

        const popoverPercentage = (
            <Popover id="popover-percentage-trigger-click-root-close">
                <div>
                    <label>Progress so far ({task.percentage} %)</label>
                    <div className="percentage">
                        <InputRange
                            maxValue={100}
                            minValue={0}
                            value={task.percentage}
                            onChange={percentage => this.props.inlineSetPercentage(task, percentage)}/>
                    </div>
                </div>
            </Popover>
        );

        const priorityIcon = () => {
            if (task.priority == 1) return "task-priority-icon text-success";

            if (task.priority == 2) return "task-priority-icon text-warning";

            if (task.priority == 3) return "task-priority-icon text-danger";

            return "task-priority-icon";
        };

        /*{
         (task.start_date && task.end_date) ?
         <div className="task-dates">(Starts {moment(task.start_date).format("MMM Do YY")} - Due {moment(task.end_date).format("MMM Do YY")})</div>
         :
         ""
         }*/

        // console.log(moment(task.start_date, 'YYYY-MM-DD HH:mm:ss').format('ddd MMM Do'), 'tasktask')
        return connectDragSource(connectDropTarget(
            <div className="task" style={{opacity}}>
                <div className="task-drag">
                    <span className="glyphicon glyphicon-sort"/>
                </div>
                <div className="task-control">
                    <div className="task-edit-icon" onClick={() => {
                        this.props.cancelAddTask();
                        this.props.editTask(task);
                    }}>
                        <div className="tooltiptext">Edit</div>
                        <i className="md md-edit"/>
                    </div>
                    <div className={(task.is_done) ? "task-status task-done" : "task-status"}>
                        <i className="md md-done" onClick={() => this.props.inlineSetDone(task)}/>
                    </div>
                    <div className="task-status task-delete m-r-10">
                        <i className="md md-clear" onClick={() => this.props.deleteTask(task)}/>
                    </div>
                    <div className="task-owner label label-inverse">{task.owner.first_name} {task.owner.last_name}</div>
                    <div className="task-title">{task.title}</div>
                    <div className="task-calendar">
                        <div className="tooltiptext">Schedule</div>
                        <OverlayTrigger trigger="click" rootClose placement="bottom" overlay={popoverCalendar}>
                            <i className="glyphicon glyphicon-calendar"/>
                        </OverlayTrigger>
                    </div>
                    <div className={priorityIcon()}>
                        <div className="tooltiptext">Priority</div>
                        <OverlayTrigger trigger="click" rootClose placement="bottom" overlay={popoverPriority}>
                            <i className="md md-error"/>
                        </OverlayTrigger>
                    </div>
                    <div className="task-percentage">
                        <div className="tooltiptext">Progress</div>
                        <OverlayTrigger trigger="click" rootClose placement="bottom" overlay={popoverPercentage}>
                            <span>{task.percentage}%</span>
                        </OverlayTrigger>
                    </div>
                </div>
                { (task.start_date != null && task.end_date != null) ?
                    <div className="task-dates">
                        (Started {moment(task.start_date, 'YYYY-MM-DD HH:mm:ss').format('ddd MMM Do')} <i
                        className="fa fa-long-arrow-right"/> <span
                        className="task-due-date">Due {moment(task.end_date, 'YYYY-MM-DD HH:mm:ss').format('ddd MMM Do')}</span>)
                    </div> : <div></div>}
            </div>
        ));
    }
}

export default Task;