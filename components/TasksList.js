import React, {Component} from 'react';
import {connect} from 'react-redux';
import Task from './Task';
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import {
    moveCard,
    init,
    sendReorder,
    addTask,
    cancelAddTask,
    titleChanged,
    datesChanged,
    ownerChanged,
    descriptionChanged,
    priorityChanged,
    percentageChanged,
    saveTask,
    editTask,
    taskDelete,
    cancelEditTask,
    editTitleChanged,
    editOwnerChanged,
    editDatesChanged,
    editDescriptionChanged,
    editPriorityChanged,
    editPercentageChanged,
    updateTask,
    inlineSetDone,
    inlineSetDates,
    inlineSetPriority,
    inlineSetPercentage
} from '../actions/tasks';
import AddTask from './AddTask';
import EditTask from './EditTask';

//@DragDropContext(HTML5Backend)
class TasksList extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.init();
    }

    componentWillUpdate(nextProps) {
        // check task order
        const tasks = nextProps.tasks.filter((task, index) => {
            if (task.id != nextProps.origin_tasks[index].id) return true;
        });

        if (nextProps.tasks.length > 0) $('.scrum-board-link').removeClass('hidden');

        if (tasks.length > 0) this.props.sendReorder(nextProps.tasks);
    }

    onDelete(task, index) {

        console.log(this.props, 'props new')
        let props = this.props;

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#4fa7f3',
            cancelButtonColor: '#d57171',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            props.taskDelete(task, index);
        }, function (dismiss) {
            console.log('dismiss')
        });
    }

    render() {
        const {tasks, new_task, users, edit_task} = this.props;

        return (
            <div>
                {tasks.map((task, i) => {
                    if (edit_task && edit_task.id == task.id) {
                        return (
                            <EditTask
                                key={task.id}
                                edit_task={this.props.edit_task}
                                users={users}
                                editTitleChanged={(title) => this.props.editTitleChanged(title)}
                                editOwnerChanged={(owner_id) => this.props.editOwnerChanged(owner_id)}
                                cancelEditTask={() => this.props.cancelEditTask()}
                                editDatesChanged={(dates) => this.props.editDatesChanged(dates)}
                                editDescriptionChanged={(description) => this.props.editDescriptionChanged(description)}
                                editPriorityChanged={(priority) => this.props.editPriorityChanged(priority)}
                                editPercentageChanged={(percentage) => this.props.editPercentageChanged(percentage)}
                                updateTask={(task) => this.props.updateTask(task)}
                                deleteTask={(task) => this.onDelete(task, i)}
                            />
                        );
                    } else {
                        return (
                            <Task
                                key={task.id}
                                index={i}
                                id={task.id}
                                task={task}
                                moveCard={(dragIndex, hoverIndex) => this.props.moveCard(dragIndex, hoverIndex)}
                                editTask={(task) => this.props.editTask(task)}
                                inlineSetDone={(task) => this.props.inlineSetDone(task)}
                                inlineSetDates={(task) => this.props.inlineSetDates(task)}
                                inlineSetPriority={(task, priority) => this.props.inlineSetPriority(task, priority)}
                                inlineSetPercentage={(task, percentage) => this.props.inlineSetPercentage(task, percentage)}
                                cancelAddTask={() => this.props.cancelAddTask()}
                                deleteTask={(task) => this.onDelete(task, i)}
                            />
                        );
                    }
                })}
                <AddTask
                    new_task={new_task}
                    users={users}
                    addTask={() => this.props.addTask()}
                    cancelAddTask={() => this.props.cancelAddTask()}
                    titleChanged={(title) => this.props.titleChanged(title)}
                    datesChanged={(dates) => this.props.datesChanged(dates)}
                    ownerChanged={(owner_id) => this.props.ownerChanged(owner_id)}
                    descriptionChanged={(description) => this.props.descriptionChanged(description)}
                    priorityChanged={(priority) => this.props.priorityChanged(priority)}
                    percentageChanged={(percentage) => this.props.percentageChanged(percentage)}
                    saveTask={(task) => this.props.saveTask(task)}
                    cancelEditTask={() => this.props.cancelEditTask()}
                />
            </div>
        );
    }
}

const mapStateToProps = state => state;

TasksList = DragDropContext(HTML5Backend)(TasksList);
TasksList = connect(mapStateToProps, {
    moveCard,
    init,
    sendReorder,
    addTask,
    cancelAddTask,
    titleChanged,
    datesChanged,
    ownerChanged,
    descriptionChanged,
    priorityChanged,
    percentageChanged,
    saveTask,
    editTask,
    taskDelete,
    cancelEditTask,
    editTitleChanged,
    editOwnerChanged,
    editDatesChanged,
    editDescriptionChanged,
    editPriorityChanged,
    editPercentageChanged,
    updateTask,
    inlineSetDone,
    inlineSetDates,
    inlineSetPriority,
    inlineSetPercentage
})(TasksList);

export default TasksList;