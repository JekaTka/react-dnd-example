import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import tasks from './reducers/tasks';
import TasksList from './components/TasksList';

const store = createStore(tasks, applyMiddleware(thunkMiddleware));

ReactDOM.render(
    <Provider store={store} >
        <TasksList />
    </Provider>,
    document.getElementById('tasks-list')
);