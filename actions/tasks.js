import axios from 'axios';

const reorder = (tasks) => ({
    type: 'REORDER',
    tasks
});

export const sendReorder = (tasks) => {
    const ids = tasks.map(task => task.id);
    return dispatch => {
        return axios.post("/projects/"+window.location.pathname.split('/')[2]+"/reorder", {ids})
            .then(response => {
                dispatch(reorder(tasks));
            });
    };
};

export const moveCard = (dragIndex, hoverIndex) => ({
    type: 'MOVE_CARD',
    dragIndex,
    hoverIndex
});

const receiveInitData = ({tasks, users}) => ({
    type: 'RECEIVE_INIT_DATA',
    tasks,
    users
});

export const init = () => {
    return dispatch => {
        return axios.get('/projects/'+window.location.pathname.split('/')[2]+'/project-tasks')
            .then(response => {
                dispatch(receiveInitData(response.data));
            });
    };
};

export const addTask = () => ({
    type: 'ADD_TASK'
});

export const deleteTask = (task, index) => ({
    type: 'DELETE_TASK',
    index: index
});

export const cancelAddTask = () => ({
    type: 'CANCEL_ADD_TASK'
});

export const titleChanged = (title) => ({
    type: 'TITLE_CHANGED',
    title
});

export const datesChanged = (dates) => ({
    type: 'DATES_CHANGED',
    dates
});

export const ownerChanged = (owner_id) => ({
    type: 'OWNER_CHANGED',
    owner_id
});

export const descriptionChanged = (description) => ({
    type: 'DESCRIPTION_CHANGED',
    description
});

export const priorityChanged = (priority) => ({
    type: 'PRIORITY_CHANGED',
    priority
});

export const percentageChanged = (percentage) => ({
    type: 'PERCENTAGE_CHANGED',
    percentage
});

const titleError = () => ({
    type: 'TITLE_ERROR'
});

const taskSaved = (task) => ({
    type: 'TASK_SAVED',
    task
});

export const saveTask = (task) => {
    return dispatch => {
        if(task.title) {
            return axios.post('/tasks', task)
                .then(response => {
                    dispatch(taskSaved(response.data));
                });
        } else {
            dispatch(titleError());
        }
    };
};

export const editTask = (task) => ({
    type: 'EDIT_TASK',
    task
});

export const cancelEditTask = () => ({
    type: 'CANCEL_EDIT_TASK'
});

export const editTitleChanged = (title) => ({
    type: 'EDIT_TITLE_CHANGED',
    title
});

export const editOwnerChanged = (owner_id) => ({
    type: 'EDIT_OWNER_CHANGED',
    owner_id
});

export const editDatesChanged = (dates) => ({
    type: 'EDIT_DATES_CHANGED',
    dates
});

export const editDescriptionChanged = (description) => ({
    type: 'EDIT_DESCRIPTION_CHANGED',
    description
});

export const editPriorityChanged = (priority) => ({
    type: 'EDIT_PRIORITY_CHANGED',
    priority
});

export const editPercentageChanged = (percentage) => ({
    type: 'EDIT_PERCENTAGE_CHANGED',
    percentage
});

const taskUpdated = (task) => ({
    type: 'TASK_UPDATED',
    task
});

const editTitleError = () => ({
    type: 'EDIT_TITLE_ERROR'
});

export const updateTask = (task) => {
    return dispatch => {
        if(task.title) {

            return axios.put('/tasks/'+task.id, task)
                .then(response => {
                    dispatch(taskUpdated(response.data));
                });
        } else {
            dispatch(editTitleError());
        }
    };
};

export const inlineSetDone = (task) => {
    return dispatch => {
        let is_done = (parseInt(task.is_done) == 0) ? 1 : 0;
        let percentage = (is_done == 1) ? 100 : 50;

        task = {...task, is_done, percentage };

        let updateTask = {
            id: task.id,
            is_done: task.is_done
        };
        dispatch(taskUpdated(task));
        return axios.put('/tasks/'+task.id, updateTask);
    };
};

export const inlineSetDates = (task) => {
    return dispatch => {

        return axios.put('/tasks/'+task.id, task)
            .then(response => {
                dispatch(taskUpdated(response.data));
            });
    };
};

export const inlineSetPriority = (task, priority) => {
    return dispatch => {
        task = {...task, priority};

        dispatch(taskUpdated(task));

        return axios.put('/tasks/'+task.id, {id: task.id, priority});
    };
};

export const inlineSetPercentage = (task, percentage) => {
    return dispatch => {
        task = {...task, percentage};

        dispatch(taskUpdated(task));

        return axios.put('/tasks/'+task.id, {id: task.id, percentage});
    };
};

export const taskDelete = (task, index) => {
    return dispatch => {
        task = {...task};
        console.log(task, 'task')

        dispatch(deleteTask(task, index));

        return axios.delete('/tasks/'+task.id, {id: task.id});
    };
};