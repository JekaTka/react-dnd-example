const models = (state = {
    tasks: [],
    origin_tasks: [],
    users: [],
    edit_task: null,
    project_id: 0,
    new_task: {
        title: '',
        title_error: false,
        project_id: 0,
        description: '',
        priority: 0,
        percentage: 0,
        from: null,
        to: null,
        start_date: null,
        end_date: null,
        open: false
    }
}, action) => {
    switch (action.type) {
        case 'MOVE_CARD':
            const { tasks } = state;
            const dragCard = tasks[action.dragIndex];
            // delete draggable element
            let newTasks = [
                ...tasks.slice(0, action.dragIndex),
                ...tasks.slice(action.dragIndex + 1)
            ];

            // add draggable element to new index
            newTasks = [
                ...newTasks.slice(0, action.hoverIndex),
                dragCard,
                ...newTasks.slice(action.hoverIndex)
            ];
            return {
                ...state,
                tasks: newTasks
            };

        case 'REORDER':
            return {
                ...state,
                origin_tasks: action.tasks
            };

        case 'RECEIVE_INIT_DATA':
            const project_id = window.location.pathname.split('/')[2];
            return {
                ...state,
                tasks: action.tasks,
                users: action.users,
                origin_tasks: action.tasks,
                project_id,
                new_task: {
                    ...state.new_task,
                    project_id
                }
            };

        case 'ADD_TASK':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    owner_id: state.users[0].id,
                    open: true
                }
            };

        case 'DELETE_TASK':

            let tasksDel = state.tasks;
            tasksDel = [...tasksDel.slice(0,action.index), ...tasksDel.slice(action.index+1)];

            return {
                ...state,
                tasks: tasksDel
            };

        case 'CANCEL_ADD_TASK':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    open: false
                }
            };

        case 'TITLE_CHANGED':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    title: action.title
                }
            };

        case 'DATES_CHANGED':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    from: action.dates.from,
                    to: action.dates.to,
                    start_date: action.dates.start_date,
                    end_date: action.dates.end_date
                }
            };

        case 'OWNER_CHANGED':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    owner_id: action.owner_id
                }
            };

        case 'DESCRIPTION_CHANGED':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    description: action.description
                }
            };

        case 'PRIORITY_CHANGED':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    priority: action.priority
                }
            };

        case 'PERCENTAGE_CHANGED':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    percentage: action.percentage
                }
            };

        case 'TITLE_ERROR':
            return {
                ...state,
                new_task: {
                    ...state.new_task,
                    title_error: true
                }
            };

        case 'TASK_SAVED':
            return {
                ...state,
                tasks: [...state.tasks, action.task],
                origin_tasks: [...state.tasks, action.task],
                new_task: {
                    ...state.new_task,
                    title: '',
                    title_error: false,
                    description: '',
                    priority: 0,
                    percentage: 0,
                    from: null,
                    to: null,
                    start_date: null,
                    end_date: null,
                    open: false
                }
            };

        case 'EDIT_TASK':
            const edit_task = action.task;

            return {
                ...state,
                edit_task: {
                    ...edit_task,
                    title_error: false,
                    start_date: (edit_task.start_date) ? moment(edit_task.start_date).format("DD/MM/YYYY") : null,
                    end_date: (edit_task.end_date) ? moment(edit_task.end_date).format("DD/MM/YYYY") : null,
                    from: (edit_task.start_date) ? moment(edit_task.start_date)._d : null,
                    to: (edit_task.end_date) ? moment(edit_task.end_date)._d : null
                }
            };

        case 'CANCEL_EDIT_TASK':
            return {
                ...state,
                edit_task: null
            };

        case 'EDIT_TITLE_CHANGED':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    title: action.title,
                    title_error: false
                }
            };

        case 'EDIT_OWNER_CHANGED':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    owner_id: action.owner_id
                }
            };

        case 'EDIT_DATES_CHANGED':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    from: action.dates.from,
                    to: action.dates.to,
                    start_date: action.dates.start_date,
                    end_date: action.dates.end_date
                }
            };

        case 'EDIT_DESCRIPTION_CHANGED':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    description: action.description
                }
            };

        case 'EDIT_PRIORITY_CHANGED':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    priority: action.priority
                }
            };

        case 'EDIT_PERCENTAGE_CHANGED':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    percentage: action.percentage
                }
            };

        case 'EDIT_TITLE_ERROR':
            return {
                ...state,
                edit_task: {
                    ...state.edit_task,
                    title_error: true
                }
            };

        case 'TASK_UPDATED':
            let tasksTmp = state.tasks.map(task => {
                if(task.id == action.task.id){
                    return action.task;
                }

                return task;
            });

            return {
                ...state,
                tasks: tasksTmp,
                origin_tasks: tasksTmp,
                edit_task: null
            };

        default:
            return state;
    }
};

export default models;